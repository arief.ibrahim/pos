package com.mentoring.pos.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mentoring.pos.helpers.Response;
import com.mentoring.pos.helpers.dtos.ProductDto;
import com.mentoring.pos.services.IProductSvc;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    @Autowired
    private IProductSvc productSvc;

    @GetMapping
    public ResponseEntity<Response<?>> findAllProducts() {
        Response<?> response = new Response<>(200, "success", productSvc.findAllProducts());

        return  ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
		// return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Response<?>> findById(@PathVariable("id") Long id) {
        Response<?> response = new Response<>(200, "success", productSvc.findById(id));

        return  ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @GetMapping(params = "code")
    public ResponseEntity<Response<?>> findByCode(@RequestParam(value = "code") String code) {
        Response<?> response = new Response<>(200, "success", productSvc.findByCode(code));

        return  ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @PostMapping
    public ResponseEntity<Response<?>> save(@RequestBody ProductDto dto) {
        productSvc.save(dto);

        Response<?> response = new Response<>(201, "success", dto);

        return  ResponseEntity
                .status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @PutMapping
    public ResponseEntity<Response<?>> update(@RequestBody ProductDto dto) {
        productSvc.update(dto);

        Response<?> response = new Response<>(200, "success", dto);

        return  ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Response<?>> delete(@PathVariable("id") Long id) {
        productSvc.delete(id);

        Response<?> response = new Response<>(204, "success");

        return  ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }
}
