package com.mentoring.pos.controllers;

import com.mentoring.pos.helpers.Response;
import com.mentoring.pos.helpers.dtos.OrderDto;
import com.mentoring.pos.services.IOrderSvc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/orders")
public class OrderController {

    @Autowired
    private IOrderSvc orderSvc;

    @GetMapping
    public ResponseEntity<Response<?>> findAllOrders() {
        Response<?> response = new Response<>(201, "success", orderSvc.findAllOrders());

        return  ResponseEntity
                .status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Response<?>> findOrderById(@PathVariable("id") Long id) {
        Response<?> response = new Response<>(200, "success", orderSvc.findOrderById(id));

        return  ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @PostMapping
    public ResponseEntity<Response<?>> save(@RequestBody OrderDto dto) {
        orderSvc.save(dto);

        Response<?> response = new Response<>(204, "success", dto);

        return  ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }
}
