package com.mentoring.pos.controllers;

import com.mentoring.pos.helpers.Response;
import com.mentoring.pos.helpers.dtos.CategoryDto;
import com.mentoring.pos.services.ICategorySvc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/categories")
public class CategoryController {

    @Autowired
    private ICategorySvc categorySvc;

    @GetMapping
    public ResponseEntity<Response<?>> findAllCategories() {
        Response<?> response = new Response<>(200, "success", categorySvc.findAllCategories());

        return  ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Response<?>> findById(@PathVariable("id") Long id) {
        Response<?> response = new Response<>(200, "success", categorySvc.findById(id));

        return  ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @PostMapping
    public ResponseEntity<Response<?>> save(@RequestBody CategoryDto dto) {
        categorySvc.save(dto);

        Response<?> response = new Response<>(201, "success", dto);

        return  ResponseEntity
                .status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @PutMapping
    public ResponseEntity<Response<?>> update(@RequestBody CategoryDto dto) {
        categorySvc.update(dto);

        Response<?> response = new Response<>(200, "success", dto);

        return  ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Response<?>> delete(@PathVariable("id") Long id) {
        categorySvc.delete(id);

        Response<?> response = new Response<>(204, "success");

        return  ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }
}
