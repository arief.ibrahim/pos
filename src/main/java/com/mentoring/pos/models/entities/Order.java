package com.mentoring.pos.models.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "trx_orders")
public class Order implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6656628978842525981L;
	
	
	@Id
    @Column(name = "order_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderId;
    @Column(name = "customer_id")
    private Long customerId;
    @Column(name = "user_id")
    private Long userId;
    @Column(name = "invoice")
    private String invoice;
    @Column(name = "total")
    private BigDecimal total;
    @Column(name = "date")
    private LocalDateTime date;

    public Order() {
    }

    public Order(Long orderId, Long customerId, Long userId, String invoice, BigDecimal total, LocalDateTime date) {
        this.orderId = orderId;
        this.customerId = customerId;
        this.userId = userId;
        this.invoice = invoice;
        this.total = total;
        this.date = date;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
