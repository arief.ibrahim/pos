package com.mentoring.pos.models.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "mst_categories")
public class Category implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -5806886929139833597L;
	
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_id")
    private Long categoryId;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "delete_status")
    private Integer deleteStatus;

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(Integer deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    public Category() {
    }

    public Category(Long categoryId, String name, String description, Integer deleteStatus) {
        this.categoryId = categoryId;
        this.name = name;
        this.description = description;
        this.deleteStatus = deleteStatus;
    }
}
