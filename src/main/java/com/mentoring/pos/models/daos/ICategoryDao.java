package com.mentoring.pos.models.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mentoring.pos.models.entities.Category;

public interface ICategoryDao extends JpaRepository<Category, Long> {

	@Query("SELECT cat FROM Category cat WHERE cat.deleteStatus = 0")
	List<Object[]> findAllCategories();
}
