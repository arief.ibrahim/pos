package com.mentoring.pos.models.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mentoring.pos.models.entities.Product;

public interface IProductDao extends JpaRepository<Product, Long> {

	@Query("SELECT prd, cat FROM Product prd JOIN Category cat ON prd.categoryId = cat.categoryId "
			+ "WHERE prd.code = :code AND prd.deleteStatus = 0 AND cat.deleteStatus = 0")
	List<Object[]> findByCode(@Param("code") String code);

	@Query("SELECT prd, cat FROM Product prd JOIN Category cat ON prd.categoryId = cat.categoryId "
			+ "WHERE prd.deleteStatus = 0 AND cat.deleteStatus = 0")
	List<Object[]> findAllProducts();
}
