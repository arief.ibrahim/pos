package com.mentoring.pos.models.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mentoring.pos.models.entities.Order;

public interface IOrderDao extends JpaRepository<Order, Long> {

	@Query("SELECT ord, orddtl, cust, usr, prd FROM Order ord "
			+ "JOIN OrderDetail orddtl ON ord.orderId = orddtl.orderId "
			+ "JOIN Customer cust ON ord.customerId = cust.customerId " + "JOIN User usr ON ord.userId = usr.userId "
			+ "JOIN Product prd ON orddtl.productId = prd.productId")
	List<Object[]> findAllOrders();

	@Query("SELECT ord, orddtl, cust, usr, prd FROM Order ord "
			+ "JOIN OrderDetail orddtl ON ord.orderId = orddtl.orderId "
			+ "JOIN Customer cust ON ord.customerId = cust.customerId " + "JOIN User usr ON ord.userId = usr.userId "
			+ "JOIN Product prd ON orddtl.productId = prd.productId " + "WHERE ord.orderId = :id")
	List<Object[]> findOrderById(@Param("id") Long id);
}
