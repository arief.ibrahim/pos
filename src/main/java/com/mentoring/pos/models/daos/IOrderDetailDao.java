package com.mentoring.pos.models.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mentoring.pos.models.entities.OrderDetail;

public interface IOrderDetailDao extends JpaRepository<OrderDetail, Long> {
}
