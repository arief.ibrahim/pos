package com.mentoring.pos.services;

import com.mentoring.pos.helpers.dtos.CategoryDto;

import java.util.List;

public interface ICategorySvc {

    List<CategoryDto> findAllCategories();
    CategoryDto findById(Long id);
    void save(CategoryDto categoryDto);
    void update(CategoryDto categoryDto);
    void delete(Long id);
}
