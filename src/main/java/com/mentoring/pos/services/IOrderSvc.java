package com.mentoring.pos.services;

import com.mentoring.pos.helpers.dtos.OrderDto;

import java.util.List;

public interface IOrderSvc {

    List<OrderDto> findAllOrders();
    List<OrderDto> findOrderById(Long id);
    void save(OrderDto orderDto);
}
