package com.mentoring.pos.services;

import com.mentoring.pos.helpers.dtos.OrderDetailDto;

import java.util.List;

public interface IOrderDetailSvc {

    List<OrderDetailDto> findAllOrderDetails();
    OrderDetailDto findById(Long id);
    void save(OrderDetailDto orderDetailDto);
    void update(OrderDetailDto orderDetailDto);
    void delete(Long id);
}
