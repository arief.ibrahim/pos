package com.mentoring.pos.services;

import com.mentoring.pos.helpers.dtos.ProductDto;

import java.util.List;

public interface IProductSvc {

    List<ProductDto> findAllProducts();
    ProductDto findById(Long id);
    List<ProductDto> findByCode(String productCode);
    void save(ProductDto productDto);
    void update(ProductDto productDto);
    void delete(Long id);
}
