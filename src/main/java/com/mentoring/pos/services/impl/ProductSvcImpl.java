package com.mentoring.pos.services.impl;

import com.mentoring.pos.helpers.dtos.ProductDto;
import com.mentoring.pos.models.daos.ICategoryDao;
import com.mentoring.pos.models.entities.Category;
import com.mentoring.pos.models.entities.Product;
import com.mentoring.pos.models.daos.IProductDao;
import com.mentoring.pos.services.IProductSvc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("IProductSvc")
@Transactional
public class ProductSvcImpl implements IProductSvc {

    @Autowired
    private IProductDao productDao;

    @Autowired
    private ICategoryDao categoryDao;

    @Override
    public List<ProductDto> findAllProducts() {
        List<ProductDto> listDtos = new ArrayList<>();
        List<Object[]> listObjs = productDao.findAllProducts();

        for (Object[] obj :
                listObjs) {
            ProductDto dto = new ProductDto();
            Product product = (Product) obj[0];
            Category category = (Category) obj[1];

            dto.setProductId(product.getProductId());
            dto.setCategoryId(product.getCategoryId());
            dto.setCategoryName(category.getName());
            dto.setProductCode(product.getCode());
            dto.setProductName(product.getName());
            dto.setProductPrice(product.getPrice());
            dto.setProductStock(product.getStock());
            dto.setProductDescription(product.getDescription());

            listDtos.add(dto);
        }

        return listDtos;
    }

    public ProductDto findById(Long id) {
        Optional<Product> optPrd = productDao.findById(id);

        if (!optPrd.isPresent())
            return null;

        Product product = optPrd.get();
        Optional<Category> optCat = categoryDao.findById(product.getCategoryId());

        if (!optCat.isPresent())
            return null;

        Category category = optCat.get();

        ProductDto dto = new ProductDto();

        dto.setProductId(product.getProductId());
        dto.setCategoryId(product.getCategoryId());
        dto.setCategoryName(category.getName());
        dto.setProductCode(product.getCode());
        dto.setProductName(product.getName());
        dto.setProductPrice(product.getPrice());
        dto.setProductStock(product.getStock());
        dto.setProductDescription(product.getDescription());

        return dto;
    }

    public List<ProductDto> findByCode(String productCode) {
        List<ProductDto> listDtos = new ArrayList<>();
        List<Object[]> listObjs = productDao.findByCode(productCode);

        for (Object[] obj :
                listObjs) {
            ProductDto dto = new ProductDto();
            Product product = (Product) obj[0];
            Category category = (Category) obj[1];

            dto.setProductId(product.getProductId());
            dto.setCategoryId(product.getCategoryId());
            dto.setCategoryName(category.getName());
            dto.setProductCode(product.getCode());
            dto.setProductName(product.getName());
            dto.setProductPrice(product.getPrice());
            dto.setProductStock(product.getStock());
            dto.setProductDescription(product.getDescription());

            listDtos.add(dto);
        }

        return listDtos;
    }

    @Override
    public void save(ProductDto productDto) {
        Product product = new Product();

        product.setCategoryId(productDto.getCategoryId());
        product.setCode(productDto.getProductCode());
        product.setName(productDto.getProductName());
        product.setStock(productDto.getProductStock());
        product.setPrice(productDto.getProductPrice());
        product.setDescription(productDto.getProductDescription());
        product.setDeleteStatus(0);

        productDao.save(product);
    }

    @Override
    public void update(ProductDto productDto) {
        Product product = productDao.getReferenceById(productDto.getProductId());

        product.setCategoryId(productDto.getCategoryId());
        product.setCode(productDto.getProductCode());
        product.setName(productDto.getProductName());
        product.setStock(productDto.getProductStock());
        product.setPrice(productDto.getProductPrice());
        product.setDescription(productDto.getProductDescription());
        product.setDeleteStatus(0);

        productDao.save(product);
    }

    @Override
    public void delete(Long id) {
        Product product = productDao.getReferenceById(id);
        product.setDeleteStatus(1);

        productDao.save(product);
    }
}
