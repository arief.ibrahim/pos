package com.mentoring.pos.services.impl;

import com.mentoring.pos.helpers.dtos.OrderDetailDto;
import com.mentoring.pos.services.IOrderDetailSvc;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("IOrderDetailSvc")
@Transactional
public class OrderDetailSvcImpl implements IOrderDetailSvc {

    @Override
    public List<OrderDetailDto> findAllOrderDetails() {
        return null;
    }

    @Override
    public OrderDetailDto findById(Long id) {
        return null;
    }

    @Override
    public void save(OrderDetailDto orderDetailDto) {

    }

    @Override
    public void update(OrderDetailDto orderDetailDto) {

    }

    @Override
    public void delete(Long id) {

    }
}
