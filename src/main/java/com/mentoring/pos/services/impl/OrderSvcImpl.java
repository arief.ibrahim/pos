package com.mentoring.pos.services.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mentoring.pos.helpers.dtos.OrderDetailDto;
import com.mentoring.pos.helpers.dtos.OrderDto;
import com.mentoring.pos.models.daos.IOrderDao;
import com.mentoring.pos.models.daos.IOrderDetailDao;
import com.mentoring.pos.models.entities.Customer;
import com.mentoring.pos.models.entities.Order;
import com.mentoring.pos.models.entities.OrderDetail;
import com.mentoring.pos.models.entities.Product;
import com.mentoring.pos.models.entities.User;
import com.mentoring.pos.services.IOrderSvc;

@Service("IOrderSvc")
@Transactional
public class OrderSvcImpl implements IOrderSvc {

    @Autowired
    private IOrderDao orderDao;

    @Autowired
    private IOrderDetailDao orderDetailDao;

    @Override
    public List<OrderDto> findAllOrders() {
        List<OrderDto> listDtos = new ArrayList<>();
        List<Object[]> listObjs = orderDao.findAllOrders();

        for (Object[] obj :
                listObjs) {
            OrderDto orderDto = new OrderDto();
            OrderDetailDto orderDetailDto = new OrderDetailDto();

            Order order = (Order) obj[0];
            OrderDetail orderDetail = (OrderDetail) obj[1];
            Customer customer = (Customer) obj[2];
            User user = (User) obj[3];
            Product product = (Product) obj[4];

            orderDetailDto.setOrderDetailId(orderDetail.getOrderDetailId());
            orderDetailDto.setProductId(orderDetail.getProductId());
            orderDetailDto.setProductName(product.getName());
            orderDetailDto.setPrice(orderDetail.getPrice());
            orderDetailDto.setQty(orderDetail.getQty());

            orderDto.setOrderId(order.getOrderId());
            orderDto.setOrderDetail(orderDetailDto);
            orderDto.setCustomerId(order.getCustomerId());
            orderDto.setCustomerName(customer.getName());
            orderDto.setUserId(order.getUserId());
            orderDto.setUserName(user.getName());
            orderDto.setInvoice(order.getInvoice());
            orderDto.setTotal(order.getTotal());
            orderDto.setDate(order.getDate());

            listDtos.add(orderDto);
        }

        return listDtos;
    }

    @Override
    public List<OrderDto> findOrderById(Long id) {
        List<OrderDto> listDtos = new ArrayList<>();
        List<Object[]> listObjs = orderDao.findOrderById(id);

        for (Object[] obj :
                listObjs) {
            OrderDto orderDto = new OrderDto();
            OrderDetailDto orderDetailDto = new OrderDetailDto();

            Order order = (Order) obj[0];
            OrderDetail orderDetail = (OrderDetail) obj[1];
            Customer customer = (Customer) obj[2];
            User user = (User) obj[3];
            Product product = (Product) obj[4];

            orderDetailDto.setOrderDetailId(orderDetail.getOrderDetailId());
            orderDetailDto.setProductId(orderDetail.getProductId());
            orderDetailDto.setProductName(product.getName());
            orderDetailDto.setPrice(orderDetail.getPrice());
            orderDetailDto.setQty(orderDetail.getQty());

            orderDto.setOrderId(order.getOrderId());
            orderDto.setOrderDetail(orderDetailDto);
            orderDto.setCustomerId(order.getCustomerId());
            orderDto.setCustomerName(customer.getName());
            orderDto.setUserId(order.getUserId());
            orderDto.setUserName(user.getName());
            orderDto.setInvoice(order.getInvoice());
            orderDto.setTotal(order.getTotal());
            orderDto.setDate(order.getDate());

            listDtos.add(orderDto);
        }

        return listDtos;
    }

    @Override
    public void save(OrderDto orderDto) {
        Order order = new Order();

        OrderDetailDto orderDetailDto = orderDto.getOrderDetail();
        OrderDetail orderDetail = new OrderDetail();

        orderDetail.setOrderDetailId(orderDetailDto.getOrderDetailId());
        orderDetail.setOrderId(orderDto.getOrderId());
        orderDetail.setPrice(orderDetailDto.getPrice());
        orderDetail.setQty(orderDetailDto.getQty());
        orderDetail.setProductId(orderDetailDto.getProductId());

        order.setCustomerId(orderDto.getCustomerId());
        order.setInvoice("INV-");
        order.setDate(LocalDateTime.now());
        order.setTotal(orderDto.getTotal());
        order.setUserId(orderDto.getUserId());

        try {
            orderDetailDao.save(orderDetail);
            orderDao.save(order);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
