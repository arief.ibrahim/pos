package com.mentoring.pos.services.impl;

import com.mentoring.pos.helpers.dtos.CategoryDto;
import com.mentoring.pos.models.daos.ICategoryDao;
import com.mentoring.pos.models.entities.Category;
import com.mentoring.pos.services.ICategorySvc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("ICategorySvc")
@Transactional
public class CategorySvcImpl implements ICategorySvc {

    @Autowired
    private ICategoryDao categoryDao;

    @Override
    public List<CategoryDto> findAllCategories() {
        List<CategoryDto> listDtos = new ArrayList<>();
        List<Object[]> listObjs = categoryDao.findAllCategories();

        for (Object[] obj :
                listObjs) {
            CategoryDto dto = new CategoryDto();
            Category category = (Category) obj[0];

            dto.setCategoryId(category.getCategoryId());
            dto.setCategoryName(category.getName());
            dto.setCategoryDescription(category.getDescription());

            listDtos.add(dto);
        }

        return listDtos;
    }

    @Override
    public CategoryDto findById(Long id) {
        Optional<Category> optCat = categoryDao.findById(id);

        if (!optCat.isPresent())
            return null;

        Category category = optCat.get();
        CategoryDto dto = new CategoryDto();

        dto.setCategoryId(category.getCategoryId());
        dto.setCategoryName(category.getName());
        dto.setCategoryDescription(category.getDescription());

        return dto;
    }

    @Override
    public void save(CategoryDto categoryDto) {
        Category category = new Category();

        category.setName(categoryDto.getCategoryName());
        category.setDescription(categoryDto.getCategoryDescription());
        category.setDeleteStatus(0);

        categoryDao.save(category);
    }

    @Override
    public void update(CategoryDto categoryDto) {
        Category category = new Category();

        category.setCategoryId(categoryDto.getCategoryId());
        category.setName(categoryDto.getCategoryName());
        category.setDescription(categoryDto.getCategoryDescription());
        category.setDeleteStatus(0);

        categoryDao.save(category);
    }

    @Override
    public void delete(Long id) {
        Category category = categoryDao.getReferenceById(id);
        category.setDeleteStatus(1);

        categoryDao.save(category);

    }
}
