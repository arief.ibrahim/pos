package com.mentoring.pos.exception;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ErrorHandlerAdvice {

	@ExceptionHandler({ NoSuchElementException.class })
	public ResponseEntity<Map<String, String>> generalErrorHandler(NoSuchElementException e) {
		Map<String, String> errors = new HashMap<>();
		errors.put("status", "404");
		errors.put("message", "Data not found");

		return ResponseEntity.status(HttpStatus.NOT_FOUND).contentType(MediaType.APPLICATION_JSON).body(errors);
	}
}
