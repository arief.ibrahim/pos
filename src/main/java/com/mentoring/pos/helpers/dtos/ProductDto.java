package com.mentoring.pos.helpers.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class ProductDto {

    private Long productId;
    private Long categoryId;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String categoryName;
    private String productCode;
    private String productName;
    private Integer productStock;
    private BigDecimal productPrice;
    private String productDescription;

    public ProductDto() {
    }

    public ProductDto(Long productId, Long categoryId, String categoryName, String productCode, String productName,
			Integer productStock, BigDecimal productPrice, String productDescription) {
		super();
		this.productId = productId;
		this.categoryId = categoryId;
		this.categoryName = categoryName;
		this.productCode = productCode;
		this.productName = productName;
		this.productStock = productStock;
		this.productPrice = productPrice;
		this.productDescription = productDescription;
	}
    
	public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getProductStock() {
        return productStock;
    }

    public void setProductStock(Integer productStock) {
        this.productStock = productStock;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }
}
