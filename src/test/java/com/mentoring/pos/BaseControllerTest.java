package com.mentoring.pos;

import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mentoring.pos.controllers.CategoryController;
import com.mentoring.pos.controllers.OrderController;
import com.mentoring.pos.controllers.ProductController;
import com.mentoring.pos.services.ICategorySvc;
import com.mentoring.pos.services.IOrderDetailSvc;
import com.mentoring.pos.services.IOrderSvc;
import com.mentoring.pos.services.IProductSvc;

@WebMvcTest
public class BaseControllerTest {

	@Autowired
	protected MockMvc mockMvc;

	@Autowired
	protected ObjectMapper objectMapper;

	@InjectMocks
	protected ProductController productController;

	@InjectMocks
	protected CategoryController categoryController;

	@InjectMocks
	protected OrderController orderController;

	@MockBean
	protected IProductSvc productSvc;

	@MockBean
	protected ICategorySvc categorySvc;

	@MockBean
	protected IOrderSvc orderSvc;

	@MockBean
	protected IOrderDetailSvc orderDetailSvc;

}
