package com.mentoring.pos.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mentoring.pos.BaseControllerTest;
import com.mentoring.pos.helpers.dtos.ProductDto;

public class ProductControllerTest extends BaseControllerTest {

	ProductDto productDto = new ProductDto(123l, 1234l, "categoryName", "productCode", "productName", 10,
			BigDecimal.valueOf(25000), null);

	@Test
	void testCreateProductTrue() throws JsonProcessingException, Exception {
		mockMvc.perform(post("/api/products").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(productDto))).andExpect(status().isCreated()).andDo(print());
	}

	@Test
	void testShowProductsTrue() throws JsonProcessingException, Exception {
		mockMvc.perform(get("/api/products").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andDo(print());
	}
}
